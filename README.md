# Velistic Judging App


## DEPRECATED ##

:no_entry_sign: This application was used for Velistic 2013, but will not be used for future competitions. Please find the updated source in the repository.

## Notice

This application is neither a good example of MVC design nor is particularly well-architected.

There's a bit of logic in the wrong place(M/C overlap), lack of data verification, possible lack of
integrity (it's good enough, but could be better) and database normalization.

