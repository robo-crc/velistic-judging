<section>
    <nav class="main-menu">
        <div style="text-align: center; margin: 20px">
            <img src="<?= base_url() ?>static/crc-logo.jpg" />
        </div>
        <ul>
            <li>
                <a href="#" class="results-button">Judging is over! Thanks :)</a>
            </li>
            <li>
                <a href="<?= base_url() ?>results/" class="results-button">Results</a>
            </li>
        </ul>
    </nav>
</section>